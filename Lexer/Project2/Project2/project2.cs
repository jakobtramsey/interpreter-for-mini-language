﻿using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;


namespace Analyzer
{

    class Program
    {

        public static void Main(string[] args)
        {
            Console.WriteLine("Please enter the file path of the file to be read:");
            Globals.choice = Console.ReadLine();
            try
            {
                string line;
                StreamReader file = new StreamReader(Globals.choice);
                while ((line = file.ReadLine()) != null)
                {
                    Globals.fileContents.Add(line);
                }


                Globals.sReader = new StringReader(Globals.fileContents[Globals.lineCount]);
                Globals.token = "";

                List<string> tokensList = new List<string>();
                Next();
                program();

                Console.WriteLine("Finished Syntax Analysis");
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("The File entered does not exist:");
                Console.WriteLine("Exiting in 5 seconds");
                Thread.Sleep(5000);

            }
            catch (UnauthorizedAccessException)
            {
                Console.WriteLine("Unauthorized Access to Directory or File:");
                Console.WriteLine("Exiting in 5 seconds");
                Thread.Sleep(5000);

            }
            catch (Exception)
            {

            }

            Console.WriteLine("Press Enter Key to Exit:");
            Console.ReadLine();

        }




        public static void program() //Start Analyzing the Program
        {
            Match("program");
            if (Kind() == "ID")
            {
                Match(Globals.token);
            }
            else
            {
                Expected("ID");
            }
            Match(":");
            Body();
            Match("end");
            Expected("end-of-text");
        }

        public static void Body() //Analyze Body based on grammar
        {
            if (Globals.token.Equals("bool") || Globals.token.Equals("int"))
            {
                Declarations();
            }
            Statements();

        }

        public static void Declarations() //Analyze Declarations based on grammar
        {
            Declaration();
            while (Globals.token.Equals("bool") || Globals.token.Equals("int"))
            {
                Declaration();
            }

        }

        public static void Declaration() //Analyze Declaration based on grammar
        {
            Next();
            if (Globals.token == "")
            {
                while (Globals.token == "")
                {
                    Next();
                }

            }

            if (Globals.strList.Contains(Globals.token))
            {
                Match(Globals.token);
            }
            else
            {
                Expected("ID");
            }
            Match(";");
        }

        public static void Statements() //Analyze statements based on grammar
        {
            Statement();
            while (Globals.token.Equals(";"))
            {
                Next();
                if (Globals.token == "")
                {
                    while (Globals.token == "")
                    {
                        Next();
                    }

                }
                Statement();
            }

        }

        public static void Statement() //Analyze statement based on grammar
        {
            if (Globals.token.Equals("if"))
            {
                ConditionalStatement();
            }
            else if (Globals.token.Equals("while"))
            {
                IterativeStatement();
            }
            else if (Kind() == "ID")
            {
                AssignmentStatement();
            }
            else if (Globals.token.Equals("print"))
            {
                printStatement();
            }
            else
            {
                Expected("if", "while", "ID", "print");
            }

        }

        public static void ConditionalStatement() //Analyze Conditionals based on grammar
        {
            Match("if");
            Expression();
            Match("then");
            Body();
            if (Globals.token == "else")
            {
                Next();

                if (Globals.token == "")
                {
                    while (Globals.token == "")
                    {
                        Next();
                    }
                }
                Body();
            }
            Match("fi");
        }

        public static void IterativeStatement() //Analyze Iteratives based on grammar
        {
            Match("while");
            Expression();
            Match("do");
            Body();
            Match("od");

        }

        public static void AssignmentStatement() //Analyze Assignments based on grammar
        {
            Next();
            Match(":=");
            Expression();

        }

        public static void printStatement() //Analyze Prints based on grammar
        {
            Match("print");
            Expression();

        }

        public static void Expression() //Analyze Expression based on grammar
        {
            SimpleExpression();
            List<string> exp = new List<string>() { "<", ">", "=<", ">=", "=", "!=" };
            if (exp.Contains(Globals.token))
            {
                Next();
                if (Globals.token == "")
                {
                    while (Globals.token == "")
                    {
                        Next();
                    }
                }
                SimpleExpression();
            }
            else
            {

            }
        }

        public static void SimpleExpression() //Analyze SimpleExpression based on grammar
        {
            Term();
            while (Globals.token.Equals("+") || Globals.token.Equals("-") || Globals.token.Equals("or"))
            {
                Next();
                if (Globals.token == "")
                {
                    while (Globals.token == "")
                    {
                        Next();
                    }

                }
                Term();
            }

        }

        public static void Term() //Analyze Term based on grammar
        {
            Factor();
            while (Globals.token.Equals("*") || Globals.token.Equals("/") || Globals.token.Equals("and"))
            {
                Next();
                if (Globals.token == "")
                {
                    while (Globals.token == "")
                    {
                        Next();
                    }

                }
                Factor();
            }
        }

        public static void Factor() //Analyze Factor based on grammar
        {
            if (Globals.token.Equals("-") || Globals.token.Equals("not"))
            {
                Next();
                if (Globals.token == "")
                {
                    while (Globals.token == "")
                    {
                        Next();
                    }

                }
            }
            if (Globals.token.Equals("false") || Globals.token.Equals("true") || Kind() == "NUM")
            {
                Next();
                if (Globals.token == "")
                {
                    while (Globals.token == "")
                    {
                        Next();
                    }

                }
            }
            else if (Kind() == "ID")
            {
                Next();
                if (Globals.token == "")
                {
                    while (Globals.token == "")
                    {
                        Next();
                    }

                }
            }
            else if (Globals.token.Equals("("))
            {
                Next();
                if (Globals.token == "")
                {
                    while (Globals.token == "")
                    {
                        Next();
                    }

                }
                Expression();
                Match(")");

            }

        }






        public static void Match(string t) //Match the Supplied string to the expected string
        {
            if (Globals.token == "")
            {
                while (Globals.token == "")
                {
                    Next();
                }
            }

            if (Globals.token == t) // if match is found move to next token
            {
                Next();

                if (Globals.token == "")
                {
                    while (Globals.token == "")
                    {
                        Next();
                    }
                }
            }
            else
            {
                Console.WriteLine($"Syntax Error at: {Position()}"); //If not a match return exception with syntax error
                throw new Exception("Exiting");

            }
        }

        public static void Expected(params string[] symbols) //Process error for expected symbols
        {
            for (int i = 0; i < symbols.Length; i++) //if not in supplied symbols throw error expecting the supplied symbols
            {
                if (Globals.token != symbols[i])
                {
                    Console.WriteLine($"Syntax Error at: {Position()} expected {symbols[i]}");
                    throw new Exception("Exiting");
                }
                else
                {

                }
            }
        }




        public static void Next() //Method to read the next lexeme in the text
        {
            Globals.token = "";

            if (Globals.sReader.Peek() != -1)
            {
                char cPeek = (char)Globals.sReader.Peek();
                int iPeek = Globals.sReader.Peek();

                if (Globals.letters.Contains(cPeek))
                {
                    recogID();
                }
                else if (Globals.digits.Contains(cPeek))
                {
                    recogDigit();
                }
                else
                {
                    switch (cPeek)
                    {
                        case '/':
                            recogSlash();
                            break;
                        case ':':
                            recogColon();
                            break;
                        case '=':
                            recogEqual();
                            break;
                        case ';':
                            recogSemiColon();
                            break;
                        case '+':
                            recogPlus();
                            break;
                        case '*':
                            recogMult();
                            break;
                        case '-':
                            recogMinus();
                            break;
                        case '(':
                            recogCloseParen();
                            break;
                        case ')':
                            recogOpenParen();
                            break;
                        case '<':
                            recogLThan();
                            break;
                        case '>':
                            recogGThan();
                            break;
                        case '!':
                            recogExclam();
                            break;
                        case ' ':
                            recogSpace();
                            break;
                        case '\t':
                            recogSpace();
                            break;
                        default:
                            Globals.erTk = Globals.token + (char)Globals.sReader.Peek();
                            Globals.token = "error";
                            break;
                    }
                }
            }
            else
            {
                Globals.lineCount++;
                if (Globals.lineCount < Globals.fileContents.Count)
                {
                    Globals.sReader = new StringReader(Globals.fileContents[Globals.lineCount]);
                    Globals.wordCount = 0;
                }
                else
                {
                    Globals.token = "end-of-text";
                }

            }
        }
        public static void recogID() //Method to recognize identifiers in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            Globals.wordCount++;
            while (Globals.letters.Contains((char)Globals.sReader.Peek()) || Globals.digits.Contains((char)Globals.sReader.Peek()) || (char)Globals.sReader.Peek() == '_')
            {
                Globals.token += (char)Globals.sReader.Read();
            }
            Globals.strList.Add(Globals.token);
        }
        public static void recogDigit() //Method to recognize digits in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            while (Globals.digits.Contains((char)Globals.sReader.Peek()))
            {
                Globals.token += (char)Globals.sReader.Read();
            }
            Globals.wordCount++;
        }
        public static void recogSlash() //Method to recognize / and // in the text
        {

            Globals.sReader.Read();
            if ((char)Globals.sReader.Peek() == '/')
            {
                Globals.sReader.Read();
                Globals.lineCount++;
                Globals.sReader = new StringReader(Globals.fileContents[Globals.lineCount]);
            }
            Globals.wordCount++;
        }
        public static void recogColon() //Method to recognize : in the text
        {

            Globals.token += (char)Globals.sReader.Read();

            while ((char)Globals.sReader.Peek() == '=')
            {
                Globals.token += (char)Globals.sReader.Read();
            }
            Globals.wordCount++;
        }
        public static void recogSemiColon() //Method to recognize ; in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            Globals.wordCount++;
        }
        public static void recogPlus() //Method to recognize + in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            Globals.wordCount++;
        }
        public static void recogMult() //Method to recognize * in the text
        {

            Globals.token += (char)Globals.sReader.Read();
            Globals.wordCount++;

        }
        public static void recogMinus() //Method to recognize - in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            Globals.wordCount++;
        }
        public static void recogCloseParen() //Method to recognize ) in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            Globals.wordCount++;
        }
        public static void recogOpenParen() //Method to recognize ( in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            Globals.wordCount++;
        }
        public static void recogLThan() //Method to recognize < in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            Globals.wordCount++;
        }
        public static void recogGThan() //Method to recognize > and >= in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            if ((char)Globals.sReader.Peek() == '=')
            {
                Globals.token += (char)Globals.sReader.Read();
            }
            else if ((char)Globals.sReader.Peek() != ' ')
            {
                Globals.erTk = Globals.token + (char)Globals.sReader.Peek();
                Globals.token = "error";
            }
            Globals.wordCount++;
        }
        public static void recogEqual() //Method to recognize = and =< in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            if ((char)Globals.sReader.Peek() == '<')
            {
                Globals.token += (char)Globals.sReader.Read();
            }
            else if ((char)Globals.sReader.Peek() != ' ' && !Globals.digits.Contains((char)Globals.sReader.Peek()))
            {
                Globals.erTk = Globals.token + (char)Globals.sReader.Peek();
                Globals.token = "error";
            }
            Globals.wordCount++;
        }
        public static void recogExclam() //Method to recognize !'s & !='s in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            if ((char)Globals.sReader.Peek() == '=')
            {
                Globals.token += (char)Globals.sReader.Read();
                Globals.wordCount++;
            }
            else
            {
                Globals.erTk = Globals.token + (char)Globals.sReader.Peek();
                Globals.token = "error";
            }
        }
        public static void recogSpace() //Method to recognize spaces in the text
        {
            while ((char)Globals.sReader.Peek() == ' ' || (char)Globals.sReader.Peek() == '\t')
            {
                Globals.sReader.Read();
                Globals.wordCount++;
            }
        }
        public static string Kind() //Returns the kind of the lexeme
        {
            if (Globals.keywords.Contains(Globals.token))
            {
                return Globals.token;
            }
            else if (int.TryParse(Globals.token, out int result))
            {
                return "NUM";
            }
            else if (Globals.strList.Contains(Globals.token))
            {
                return "ID";
            }
            else
            {
                return Globals.token;
            }

        }
        public static string Value() //Returns the value of the lexeme
        {
            string val;

            if (Kind() == "NUM" || Kind() == "ID")
            {
                val = Globals.token;
            }
            else if (Globals.strList.Contains(Globals.token))
            {
                val = Globals.token;
            }
            else
            {
                val = "no value";
            }

            return val;

        }
        public static string Position() //Returns the position of the lexeme.
        {
            return (Globals.lineCount + 1) + "," + (Globals.wordCount - 1);

        }

    }
    class Globals
    {

        //List of Globals variables to be used with the void methods.
        public static List<string> keywords = new List<string>() { "program", "end", "bool", "int", ":", ";", ":=", "if", "fi", "then", "else", "while", "do", "od", "print", "_", "(", ")", "<", "=<", "=", "!=", ">=", ">",
        "+", "-", "or", "*", "/", "and", "-", "not", "true", "false"  };
        public static List<char> letters = new List<char>(){ 'a' , 'b' , 'c' , 'd' , 'e' , 'f' , 'g' , 'h' , 'i' , 'j' , 'k', 'l' , 'm' , 'n' , 'o' , 'p' , 'q' , 'u' , 'r' , 's' , 't' , 'u'
        , 'v' , 'w' , 'x' , 'y' , 'z' , 'A' , 'B' , 'C' , 'D' , 'E' , 'F' , 'G' , 'H' , 'I' , 'J' , 'K' , 'L' , 'M' , 'N' , 'O' , 'P' , 'Q' , 'U' , 'R' , 'S' , 'T' , 'U'
        , 'V' , 'W' , 'X' , 'Y' , 'Z'};
        public static List<char> digits = new List<char>() { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        public static int lineCount = 0;
        public static int wordCount;
        public static List<string> fileContents = new List<string>();
        public static string token;
        public static string choice;
        public static List<string> IDs = new List<string>();
        public static List<string> NUMs = new List<string>();
        public static int listCounter = 0;
        public static StringReader sReader;
        public static int tokInt = 0;
        public static List<string> wordList = new List<string>();
        public static List<string> strList = new List<string>();
        public static string erTk;


    }

}