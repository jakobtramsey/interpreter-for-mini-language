using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;


namespace project1{

class Program{

        public static void Main(string[] args){
            Console.WriteLine("Please enter the file path of the file to be read:");
            Globals.choice = Console.ReadLine();
            try
            {
                string line;
                StreamReader file = new StreamReader(Globals.choice);
                while ((line = file.ReadLine()) != null)
                {
                    Globals.fileContents.Add(line);
                }


              Globals.sReader = new StringReader(Globals.fileContents[Globals.lineCount]);
               Globals.token = "";
                
                
                while (!Globals.token.Equals( "end-of-text"))
             {
                    Next();
                    if(Globals.token == "error")
                        {
                        Console.Write($"Error reading token at: {Position()} ' {Globals.erTk} ' is not a valid symbol");
                        Console.WriteLine();
                        break;
                    }
                    else if (Globals.token != "" && Globals.token != "end-of-text")

                    {
                        Console.Write($"{Position()} ");
                        Console.Write($"'{Kind()}' ");
                        Console.Write($"{Value()} ");
                        Console.WriteLine();
                    }



                }
            }
            catch (Exception)
            {
                Console.WriteLine("The File entered does not exist:");
                Console.WriteLine("Exiting in 5 seconds");
                Thread.Sleep(5000);

            }

            Console.WriteLine("Press Enter Key to Exit:");
            Console.ReadLine();

        }
        





        public static void Next() //Method to read the next lexeme in the text
        {
                Globals.token = "";

                if (Globals.sReader.Peek() != -1)
                {
                char cPeek = (char)Globals.sReader.Peek();
                int iPeek = Globals.sReader.Peek();

                if (Globals.letters.Contains(cPeek))
                {
                    recogID();
                }
                else if (Globals.digits.Contains(cPeek))
                {
                    recogDigit();
                }
                else {
                    switch (cPeek)
                    {
                        case '/':
                            recogSlash();
                            break;
                        case ':':
                            recogColon();
                            break;
                        case '=':
                            recogEqual();
                            break;
                        case ';':
                            recogSemiColon();
                            break;
                        case '+':
                            recogPlus();
                            break;
                        case '*':
                            recogMult();
                            break;
                        case '-':
                            recogMinus();
                            break;
                        case '(':
                            recogCloseParen();
                            break;
                        case ')':
                            recogOpenParen();
                            break;
                        case '<':
                            recogLThan();
                            break;
                        case '>':
                            recogGThan();
                            break;
                        case '!':
                            recogExclam();
                            break;
                        case ' ':
                            recogSpace();
                            break;
                        case '\t':
                            recogSpace();
                            break;
                        default:
                            Globals.erTk = Globals.token + (char)Globals.sReader.Peek();
                            Globals.token = "error";
                            break;
                    }
                } 
              }
                else
                {
                    Globals.lineCount++;
                    if (Globals.lineCount < Globals.fileContents.Count)
                    {
                        Globals.sReader = new StringReader(Globals.fileContents[Globals.lineCount]);
                    Globals.wordCount = 0;
                    }
                    else
                    {
                        Globals.token = "end-of-text";
                    }

                }
        }
        public static void recogID() //Method to recognize identifiers in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            Globals.wordCount++;
            while (Globals.letters.Contains((char)Globals.sReader.Peek()) || Globals.digits.Contains((char)Globals.sReader.Peek()) || (char) Globals.sReader.Peek() == '_')
            {
                Globals.token += (char)Globals.sReader.Read();
            }
            Globals.strList.Add(Globals.token);
        }
        public static void recogDigit() //Method to recognize digits in the text
        {
                Globals.token += (char)Globals.sReader.Read();
            while (Globals.digits.Contains((char)Globals.sReader.Peek()))
                {
                    Globals.token += (char)Globals.sReader.Read();
            } 
                Globals.wordCount++;
        }
        public static void recogSlash() //Method to recognize / and // in the text
        {

           Globals.sReader.Read();
            if ((char)Globals.sReader.Peek() == '/')
            {
              Globals.sReader.Read();
              Globals.lineCount++;
              Globals.sReader = new StringReader(Globals.fileContents[Globals.lineCount]);
            }
            Globals.wordCount++;
        }
        public static void recogColon() //Method to recognize : in the text
        {

            Globals.token += (char)Globals.sReader.Read();

            while((char)Globals.sReader.Peek() == '=')
            {
                Globals.token += (char)Globals.sReader.Read();
            }
            Globals.wordCount++;
        }
        public static void recogSemiColon() //Method to recognize ; in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            Globals.wordCount++;
        }
        public static void recogPlus() //Method to recognize + in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            Globals.wordCount++;
        }
        public static void recogMult() //Method to recognize * in the text
        {

            Globals.token += (char)Globals.sReader.Read();
            Globals.wordCount++;

        }
        public static void recogMinus() //Method to recognize - in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            Globals.wordCount++;
        }
        public static void recogCloseParen() //Method to recognize ) in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            Globals.wordCount++;
        }
        public static void recogOpenParen() //Method to recognize ( in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            Globals.wordCount++;
        }
        public static void recogLThan() //Method to recognize < in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            Globals.wordCount++;
        }
        public static void recogGThan() //Method to recognize > and >= in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            if ((char)Globals.sReader.Peek() == '=')
            {
                Globals.token += (char)Globals.sReader.Read();
            }
            else if ((char)Globals.sReader.Peek() != ' ')
            {
                Globals.erTk = Globals.token + (char)Globals.sReader.Peek();
                Globals.token = "error";
            }
            Globals.wordCount++;
        }
        public static void recogEqual() //Method to recognize = and =< in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            if ((char)Globals.sReader.Peek() == '<')
            {
                Globals.token += (char)Globals.sReader.Read();
            }
            else if ((char)Globals.sReader.Peek() != ' ' && !Globals.digits.Contains((char) Globals.sReader.Peek()))
            {
                Globals.erTk = Globals.token + (char)Globals.sReader.Peek();
                Globals.token = "error";
            }
            Globals.wordCount++;
        }
        public static void recogExclam() //Method to recognize !'s & !='s in the text
        {
            Globals.token += (char)Globals.sReader.Read();
            if ((char)Globals.sReader.Peek() == '=')
            {
                Globals.token += (char)Globals.sReader.Read();
                Globals.wordCount++;
            }
            else
            {
                Globals.erTk = Globals.token + (char)Globals.sReader.Peek();
                Globals.token = "error";
            }
        }
        public static void recogSpace() //Method to recognize spaces in the text
        {
            while ((char)Globals.sReader.Peek() == ' ' || (char) Globals.sReader.Peek() == '\t')
            {
                Globals.sReader.Read();
                Globals.wordCount++;
            }
        }
        public static string Kind() //Returns the kind of the lexeme
        {
            if (Globals.keywords.Contains(Globals.token))
            {
                return Globals.token;
            }
            else if(int.TryParse(Globals.token, out int result)){
                return "NUM";
            }
            else if (Globals.strList.Contains(Globals.token))
            {
                return "ID";
            }
            else
            {
                return Globals.token;
            }
   
        }
        public static string Value() //Returns the value of the lexeme
        {
            string val;

            if (Kind() == "NUM" || Kind() == "ID")
            {
                val = Globals.token;
            }
            else if(Globals.strList.Contains(Globals.token))
            {
                val = Globals.token;
            }
            else
            {
                val = "no value";
            }

            return val;

        }
        public static string Position() //Returns the position of the lexeme.
        {
            return (Globals.lineCount + 1)  + "," + (Globals.wordCount - 1);

        }

    }
class Globals
    {

        //List of Globals variables to be used with the void methods.
       public static List<string> keywords = new List<string>() { "program", "end", "bool", "int", ":", ";", ":=", "if", "fi", "then", "else", "while", "do", "od", "print", "_", "(", ")", "<", "=<", "=", "!=", ">=", ">",
        "+", "-", "or", "*", "/", "and", "-", "not", "true", "false"  };
       public static List<char> letters = new List<char>(){ 'a' , 'b' , 'c' , 'd' , 'e' , 'f' , 'g' , 'h' , 'i' , 'j' , 'k', 'l' , 'm' , 'n' , 'o' , 'p' , 'q' , 'u' , 'r' , 's' , 't' , 'u'
        , 'v' , 'w' , 'x' , 'y' , 'z' , 'A' , 'B' , 'C' , 'D' , 'E' , 'F' , 'G' , 'H' , 'I' , 'J' , 'K' , 'L' , 'M' , 'N' , 'O' , 'P' , 'Q' , 'U' , 'R' , 'S' , 'T' , 'U'
        , 'V' , 'W' , 'X' , 'Y' , 'Z'};
       public static List<char> digits = new List<char>() { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
       public static int lineCount = 0;
       public static int wordCount;
       public static List<string> fileContents = new List<string>();
       public static string token;
       public static string choice;
       public static List<string> IDs = new List<string>();
       public static List<string> NUMs = new List<string>();
       public static int listCounter = 0;
       public static StringReader sReader;
       public static int tokInt = 0;
       public static List<string> wordList = new List<string>();
       public static List<string> strList = new List<string>();
       public static string erTk;


    }

}